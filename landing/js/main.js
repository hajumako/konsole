var $slider1,
    $slider2,
    $slides1, 
    $slides2,
    $longbox,
    $col,
    $col1_boxwrapper,
    $col2;
$(document).ready(function() {
    $('#slider1').unslider({
        speed: 1200,                       
        dots: true,
        delay: false,
        fluid: true
    });
    
    $('#slider2').unslider({
        speed: 1200,            
        delay: 5000,            
        dots: true,
        fluid: true
    });
    
    $slider1 = $('#slider1');
    $slider2 = $('#slider2');
    
    $slides1 = $('#slider1 ul li');
    $slides2 = $('#slider2 ul li');
    
    $longbox = $('.long-box div');
    
    $col1 = $('#col1');
    $col1_boxwrapper = $('#col1 .box-wrapper');
    $col2 = $('#col2');
    
    sliderHeight();
});
$(window).resize(sliderHeight);
$(window).load(sliderHeight);

function sliderHeight() {
    h = 0; 
    $slides1.each(function() {
        h = Math.max($(this).outerHeight(true), h);
    });
    h += 20;
    $slider1.css('height', h + 'px'); 
    
    if(window.innerWidth >= 600) {
        pad = ($col2.height() - $col1_boxwrapper.height() - h ) / 2;
        $col1.css('padding-top', pad + 'px');   
    } else
        $col1.css('padding-top', '0'); 
     
    h = 0;
    $slides2.each(function() {
        h = Math.max($(this).outerHeight(true), h);
    });
    h += 90;
    $slider2.css('height', h + 'px'); 
    
    h = $longbox.height() / 2;
    $longbox.css('margin-top', '-' + h + 'px');
}
