<!DOCTYPE html>

<html lang="pl">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1.0">
    <title>Konsole.pl - portal graczy konsolowych - gry na Xbox One, PS4, PSV, 3DS</title>
    <meta name="description" content="Konsole.pl - najświeższe informacje o grach na PS4, Xbox One, PS3, Xbox 360, Nintendo Wii U, PS Vita, PSP, Nintendo 3DS, Nintendo 2DS. Zapowiedzi nadchodzących tytułów, recenzje gier i sprzętu, ciekawe artykuły, relacje z targów i eventów, gameplay'e oraz niesamowite okazje cenowe!">
    <meta name="author" content="Quellio">
    <meta name="keywords" content="konsole, gry, akcesoria, xbox one, ps4, ps3, xbox 360">

    <!--<meta name="robots" content="index,all,follow">-->
    <meta name="distribution" content="global">
    <meta name="author" content="Quellio">

    <meta property="og:site_name" content="Konsole.pl">
    <meta property="og:title" content="Konsole.pl - portal graczy konsolowych - gry na Xbox One, PS4, PSV, 3DS">
    <meta property="og:description" content="Konsole.pl - najświeższe informacje o grach na PS4, Xbox One, PS3, Xbox 360, Nintendo Wii U, PS Vita, PSP, Nintendo 3DS, Nintendo 2DS. Zapowiedzi nadchodzących tytułów, recenzje gier i sprzętu, ciekawe artykuły, relacje z targów i eventów, gameplay'e oraz niesamowite okazje cenowe!">
    <meta property="og:image" content="">
    <meta property="og:url" content="http://www.konsole.pl/">
    <meta property="og:type" content="company">

    <link rel="apple-touch-icon" sizes="57x57" href="favicons/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="favicons/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="favicons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="favicons/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="favicons/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="favicons/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="favicons/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="favicons/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="favicons/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="favicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="favicons/favicon-194x194.png" sizes="194x194">
    <link rel="icon" type="image/png" href="favicons/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="favicons/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="favicons/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="favicons/manifest.json">
    <meta name="msapplication-TileColor" content="#df2027">
    <meta name="msapplication-TileImage" content="favicons/mstile-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/media.css">
    
    <script src="js/jquery-1.10.1.min.js" type="text/javascript"></script>
    <script src="js/unslider.min.js" type="text/javascript"></script>
    <script src="js/main.js" type="text/javascript"></script>
    
    <script>
(function(e){function t(t,n,r,o){"use strict";function a(){for(var e,n=0;u.length>n;n++)u[n].href&&u[n].href.indexOf(t)>-1&&(e=!0);e?i.media=r||"all":setTimeout(a)}var i=e.document.createElement("link"),l=n||e.document.getElementsByTagName("script")[0],u=e.document.styleSheets;return i.rel="stylesheet",i.href=t,i.media="only x",i.onload=o||null,l.parentNode.insertBefore(i,l),a(),i}var n=function(r,o){"use strict";if(r&&3===r.length){var a=e.navigator,i=e.Image,l=!(!document.createElementNS||!document.createElementNS("http://www.w3.org/2000/svg","svg").createSVGRect||!document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#Image","1.1")||e.opera&&-1===a.userAgent.indexOf("Chrome")||-1!==a.userAgent.indexOf("Series40")),u=new i;u.onerror=function(){n.method="png",n.href=r[2],t(r[2])},u.onload=function(){var e=1===u.width&&1===u.height,a=r[e&&l?0:e?1:2];n.method=e&&l?"svg":e?"datapng":"png",n.href=a,t(a,null,null,o)},u.src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==",document.documentElement.className+=" grunticon"}};n.loadCSS=t,e.grunticon=n})(this);(function(e,t){"use strict";var n=t.document,r="grunticon:",o=function(e){if(n.attachEvent?"complete"===n.readyState:"loading"!==n.readyState)e();else{var t=!1;n.addEventListener("readystatechange",function(){t||(t=!0,e())},!1)}},a=function(e){return t.document.querySelector('link[href$="'+e+'"]')},c=function(e){var t,n,o,a,c,i,u={};if(t=e.sheet,!t)return u;n=t.cssRules?t.cssRules:t.rules;for(var l=0;n.length>l;l++)o=n[l].cssText,a=r+n[l].selectorText,c=o.split(");")[0].match(/US\-ASCII\,([^"']+)/),c&&c[1]&&(i=decodeURIComponent(c[1]),u[a]=i);return u},i=function(e){var t,o,a;o="data-grunticon-embed";for(var c in e)if(a=c.slice(r.length),t=n.querySelectorAll(a+"["+o+"]"),t.length)for(var i=0;t.length>i;i++)t[i].innerHTML=e[c],t[i].style.backgroundImage="none",t[i].removeAttribute(o);return t},u=function(t){"svg"===e.method&&o(function(){i(c(a(e.href))),"function"==typeof t&&t()})};e.embedIcons=i,e.getCSS=a,e.getIcons=c,e.ready=o,e.svgLoadedCallback=u,e.embedSVG=u})(grunticon,this);
			
			grunticon(["css/icons.data.svg.css", "css/icons.data.png.css", "css/icons.fallback.css"]);
    </script>
    <noscript><link href="css/icons.fallback.css" rel="stylesheet"></noscript>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-56946436-3', 'auto');
      ga('send', 'pageview');

    </script>
</head>

<body>
    <div id="header">
        <div class="wrapper">
            <div id="logo" class="icon-logo-konsole svg-bg"></div>
        </div>
    </div> 
    <div id="content">
        <div class="wrapper">
            <div id="row1" class="floatfix">
                <div id="col1">
                    <div id="slider1" class="banner gray-color">
                        <ul>
                            <li>
                                <div>
                                    <p>
                                        <b>Witaj,</b>
                                    </p> 
                                    <p>
                                        <b>fajnie, że jesteś!</b>
                                    </p> 
                                    <p>
                                        Żeby móc pozytywnie zaskoczyć Cię podczas kolejnej wizyty, w pocie czoła pracujemy nad nową odsłoną serwisu <b class="red-color">Konsole.pl</b>.
                                    </p>
                                    <p>
                                        W zamian za Twoją wyrozumiałość, obiecujemy wysłać Ci osobiste zaproszenie, tuż przed uruchomieniem serwisu – wypełnij formularz i bądź z nami na bieżąco!
                                    </p>
                                </div>
                            </li>
                            <li>
                                <div>
                                    <p>
                                        Co takiego szykujemy dla Ciebie i pozostałych graczy konsolowych? Najświeższe newsy z branży, zapowiedzi nadchodzących tytułów, recenzje gier i sprzętu, ciekawe artykuły, relacje z targów i eventów, nasze gameplay'e, niesamowite okazje cenowe oraz przydatne narzędzia.
                                    </p>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="box-wrapper floatfix">
                        <div id="console" class="box white-bg">
                            <div class="border-box">
                                <div class="icon-icon-console svg-bg"></div>
                            </div>
                        </div>
                        <div class="box red-bg"></div>
                        <div id="triangles" class="box">
                            <div class="icon-triangles svg-bg"></div>
                        </div>
                    </div>
                </div>
                <div id="col2" class="white-bg">
                    <div class="box-wrapper floatfix">
                        <div id="form" class="box">
                            <div class="border-box">
                                <div class="icon-icon-form svg-bg"></div>
                            </div>
                        </div>
                        <div class="long-box red-bg">
                            <div class="white-color">
                                <p class="min480 min600 min800">Jeżeli grasz na konsoli lub rozważasz jej zakup, Konsole.pl to miejsce stworzone dla Ciebie!</p>
                                <p>Dołącz do tętniącej życiem społeczności graczy – <b>wypełnij formularz!</b></p>
                            </div>
                        </div>
                    </div>
                    <div id="contact-form">
                        <?php include('form.php'); ?>
                    </div>
                    <p class="sub-text">Tak jak Ty, my również nie lubimy SPAM'u - o Twój adres e-mail 
zatroszczymy się jak o własny.</p>
                </div>
            </div>
            <div id="row2" class="white-bg">
                <img src="img/PS4.png" alt="Playstation4"/>
                <img src="img/XboxOne.png" alt="Xbox One"/>
                <img src="img/PS3.png" alt="Playstation3"/>
                <img src="img/Xbox360.png" alt="Xbox 360"/>
                <img src="img/Wii.png" alt="Nintendo Wii U"/>
            </div>
            <div id="row3">
                <p>Nieważne, czy grasz już na konsolach stacjonarnych najnowszej generacji (PS4, Xbox One, Wii U), czy jeszcze na ich poprzednikach (Xbox 360, PS3, Wii), czy zdecydowanie wolisz konsole przenośne (PS Vita, PSP, 3DS, 2DS) oraz jakie gry lubisz najbardziej. Zbierzemy najciekawsze i najistotniejsze dla Ciebie informacje w jednym miejscu – zbierzemy to wszystko na Konsole.pl. Koniec z przekopywaniem się przez różne serwisy polskie i zagraniczne, przepełnione pokrewnymi tematami. Nazwa zobowiązuje: Konsole i kropka (po polsku).
                </p>
            </div>
        </div>
    </div>
    <div id="footer" class="white-bg">
        <div class="wrapper">
            <div class="icon-icon-writing svg-bg"></div>
            <div class="title red-color">Dołącz do redakcji</div>
            <div id="slider2" class="banner">
                <ul>
                    <li>
                        <p>
                            Jeżeli posiadasz doświadczenie w pisaniu tekstów o grach wideo i chcesz dołączyć do zespołu redakcjnego Konsole.pl – napisz do nas koniecznie i pochwal się swoim dotychczasowym dorobkiem! Z pewnością skontaktujemy się z Tobą i omówimy możliwości współpracy.
                        </p> 
                    </li>
                    <li>
                        <p>
                            Dopiero zaczynasz przygodę z pisaniem i nie posiadasz jeszcze portfolio, ale interesujesz się branżą elektronicznej rozrywki, a gry wideo to Twoja prawdziwa pasja? Do odważnych świat należy! Załącz próbki własnych tekstów (np. news, zapowiedź, wycinek z recenzji lub artykułu) oraz kilka zdań o sobie, które przekonają nas do Twojej osoby.
                        </p> 
                    </li>
                </ul>
                <div id="contact">
                    <p>Czekamy na Twoje zgłoszenie pod adresem</p>
                    <a href="mailto:redakcja@konsole.pl">redakcja@konsole.pl</a>
                </div>
            </div>
        </div>
        <div id="copyright">
            <p>Copyright © 2015</p>
        </div>
    </div>
</body>
</html>