<div id="WFItem8779101" class="wf-formTpl">
    <form accept-charset="utf-8" action="https://app.getresponse.com/add_contact_webform.html?u=5Nu6"
    method="post">
        <div class="wf-box">
            <div id="WFIheader" class="wf-header el" style="display:  block !important;">
                <div class="actTinyMceElBodyContent">
                    <p>
                        <span>Dołącz do Nas</span>
                    </p>
                </div>
                <em class="clearfix clearer"></em>
            </div>
            <div id="WFIcenter" class="wf-body">
                <ul class="wf-sortable" id="wf-sort-id">
                    <li class="wf-name" rel="undefined" style="display:  block !important;">
                        <div class="wf-contbox">
                            <div class="wf-labelpos">
                                <label class="wf-label"></label>
                            </div>
                            <div class="wf-inputpos">
                                <input class="wf-input" type="text" name="name" data-placeholder="yes"
                                value="Imię"></input>
                            </div>
                            <em class="clearfix clearer"></em>
                        </div>
                    </li>
                    <li class="wf-email" rel="undefined" style="display:  block !important;">
                        <div class="wf-contbox">
                            <div class="wf-labelpos">
                                <label class="wf-label"></label>
                            </div>
                            <div class="wf-inputpos">
                                <input class="wf-input wf-req wf-valid__email" type="text" name="email"
                                data-placeholder="yes" value="Email"></input>
                            </div>
                            <em class="clearfix clearer"></em>
                        </div>
                    </li>
                    <li class="wf-field__0" rel="checkbox" style="display:  block !important;">
                        <div class="wf-contbox">
                            <div class="wf-labelpos">
                                <label class="wf-label" id="4867301">Platformy</label>
                            </div>
                            <div class="wf-inputpos">
                                <div class="clrB">
                                    <label>
                                        <input name="custom_Platfromy[]" class="wf-req wf-valid__required" type="checkbox" value="Playstation 4"></input>
                                        <span class="styled-ch"><span class="ch"></span></span>
                                        <span class="wf-text">Playstation 4</span>
                                    </label>
                                </div>
                                <div class="clrB">
                                    <label>
                                        <input name="custom_Platfromy[]" class="wf-req wf-valid__required" type="checkbox" value="Xbox One"></input>
                                        <span class="styled-ch"><span class="ch"></span></span>
                                        <span class="wf-text">Xbox One</span>
                                    </label>
                                </div>
                                <div class="clrB">
                                    <label>
                                        <input name="custom_Platfromy[]" class="wf-req wf-valid__required" type="checkbox" value="PS Vita"></input>
                                        <span class="styled-ch"><span class="ch"></span></span>
                                        <span class="wf-text">PS Vita</span>
                                    </label>
                                </div>
                                <div class="clrB">
                                    <label>
                                        <input name="custom_Platfromy[]" class="wf-req wf-valid__required" type="checkbox" value="Nintendo Wii U"></input>
                                        <span class="styled-ch"><span class="ch"></span></span>
                                        <span class="wf-text">Nintendo Wii U</span>
                                    </label>
                                </div>
                                <div class="clrB">
                                    <label>
                                        <input name="custom_Platfromy[]" class="wf-req wf-valid__required" type="checkbox" value="Nintendo 3DS"></input>
                                        <span class="styled-ch"><span class="ch"></span></span>
                                        <span class="wf-text">Nintendo 3DS</span>
                                    </label>
                                </div>
                                <div class="clrB">
                                    <label>
                                        <input name="custom_Platfromy[]" class="wf-req wf-valid__required" type="checkbox" value="Nintendo DS"></input>
                                        <span class="styled-ch"><span class="ch"></span></span>
                                        <span class="wf-text">Nintendo DS</span>
                                    </label>
                                </div>
                                <div class="clrB">
                                    <label>
                                        <input name="custom_Platfromy[]" class="wf-req wf-valid__required" type="checkbox" value="Playstation 3"></input>
                                        <span class="styled-ch"><span class="ch"></span></span>
                                        <span class="wf-text">Playstation 3</span>
                                        </label>
                                </div>
                                <div class="clrB">
                                    <label>
                                        <input name="custom_Platfromy[]" class="wf-req wf-valid__required" type="checkbox" value="Xbox 360"></input>
                                        <span class="styled-ch"><span class="ch"></span></span>
                                        <span class="wf-text">Xbox 360</span>
                                    </label>
                                </div>
                                <div class="clrB">
                                    <label>
                                        <input name="custom_Platfromy[]" class="wf-req wf-valid__required" type="checkbox" value="PSP"></input>
                                        <span class="styled-ch"><span class="ch"></span></span>
                                        <span class="wf-text">PSP</span>
                                    </label>
                                </div>
                                <div class="clrB">
                                    <label>
                                        <input name="custom_Platfromy[]" class="wf-req wf-valid__required" type="checkbox" value="Nintendo Wii"></input>
                                        <span class="styled-ch"><span class="ch"></span></span>
                                        <span class="wf-text">Nintendo Wii</span>
                                    </label>
                                </div>
                                <div class="clrB">
                                    <label>
                                        <input name="custom_Platfromy[]" class="wf-req wf-valid__required" type="checkbox" value="Nintendo 2DS"></input>
                                        <span class="styled-ch"><span class="ch"></span></span>
                                        <span class="wf-text">Nintendo 2DS</span>
                                    </label>
                                </div>
                                <div class="clrB">
                                    <label>
                                        <input name="custom_Platfromy[]" class="wf-req wf-valid__required" type="checkbox" value="Inne"></input>
                                        <span class="styled-ch"><span class="ch"></span></span>
                                        <span class="wf-text">Inne</span>
                                    </label>
                                </div>
                            </div>
                            <em class="clearfix clearer"></em>
                        </div>
                    </li>
                    <li class="wf-submit" rel="undefined" style="display:  block !important;">
                        <div class="wf-contbox">
                            <div class="wf-inputpos">
                                <input type="submit" class="wf-button" name="submit" value="Wyślij"></input>
                            </div>
                            <em class="clearfix clearer"></em>
                        </div>
                    </li>
                    <li class="wf-counter" rel="undefined temporary" style="display:  none !important;">
                        <div class="wf-contbox">
                            <div>
                                <span style="padding: 4px 6px 8px 24px; background: url(https://app.getresponse.com/images/core/webforms/countertemplates.png) 0% 0px no-repeat;"
                                class="wf-counterbox">
                                    <span class="wf-counterboxbg" style="padding: 4px 12px 8px 5px; background: url(https://app.getresponse.com/images/core/webforms/countertemplates.png) 100% -36px no-repeat;">
                                        <span class="wf-counterbox0" style="padding: 5px 0px;">zapisanych:</span>
                                        <span style="padding: 5px;" name="https://app.getresponse.com/display_subscribers_count.js?campaign_name=alex_405624&var=0"
                                        class="wf-counterbox1 wf-counterq">1</span>
                                        <span style="padding: 5px 0px;" class="wf-counterbox2"></span>
                                    </span>
                                </span>
                            </div>
                        </div>
                        <em class="clearfix clearer"></em>
                    </li>
                    <li class="wf-captcha" rel="undefined" style="display:  none !important;">
                        <div class="wf-contbox wf-captcha-1" id="wf-captcha-1" wf-captchaword="Wpisz tekst z obrazka:"
                        wf-captchasound="Wprowadź liczby, które usłyszysz:" wf-captchaerror="Tekst jest niepoprawny. Spróbuj ponownie."></div>
                    </li>
                    <li class="wf-privacy" rel="undefined temporary" style="display:  none !important;">
                        <div class="wf-contbox">
                            <div>
                                <a class="wf-privacy wf-privacyico" href="http://www.getresponse.pl/permission-seal?lang=pl"
                                target="_blank" style="height: 0px !important; display: inline !important;"
                                value="temporary">Dbamy o Twoją prywatność<em class="clearfix clearer"></em></a>
                            </div>
                            <em class="clearfix clearer"></em>
                        </div>
                    </li>
                    <li class="wf-poweredby" rel="undefined temporary" style="display:  none !important;">
                        <div class="wf-contbox">
                            <div>
                                <span class="wf-poweredby wf-poweredbyico" value="temporary" style="display:  none !important;">
                                    <a class="wf-poweredbylink wf-poweredby" href="http://www.getresponse.pl/"
                                    style="display:  inline !important; color: inherit !important;" target="_blank"
                                    value="temporary">Email Marketing</a>by GetResponse</span>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div id="WFIfooter" class="wf-footer el" style="height: 20px; display:  block !important;">
                <div class="actTinyMceElBodyContent"></div>
                <em class="clearfix clearer"></em>
            </div>
        </div>
        <input type="hidden" name="webform_id" value="8779101" />
    </form>
</div>
<script type="text/javascript" src="http://app.getresponse.com/view_webform.js?wid=8779101&mg_param1=1&u=5Nu6"></script>